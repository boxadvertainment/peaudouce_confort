<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('participant_id')->unsigned();
			$table->foreign('participant_id')->references('id')->on('participants')->onDelete('cascade');
            $table->string('baby_name');
            for ($i=1; $i <= 10; $i++) {
                $table->string('caption' . $i);
                $table->string('photo' . $i);
            }
            $table->tinyInteger('cover')->nullable();
            $table->boolean('banned');
            $table->integer('reports');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photos');
    }
}
