<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    public function photo()
    {
        return $this->hasOne('App\Photo');
    }
}
