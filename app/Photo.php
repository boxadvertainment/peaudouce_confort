<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function participant()
    {
        return $this->belongsTo('App\Participant');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }
}
