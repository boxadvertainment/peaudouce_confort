<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }

    public function voter()
    {
        return $this->belongsTo('App\Voter');
    }
}
