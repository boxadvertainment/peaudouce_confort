<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['domain' => 'mail.la7dha.tn'], function () {
    Route::match(['get', 'post'],'/', 'AppController@homeMobile');
    // Route::get('showPhoto/{id}', 'AppController@showPhotoMobile');
    // Route::match(['get', 'post'], 'coverPhoto', 'AppController@coverPhotoMobile');
});

Route::group(['domain' => 'www.la7dha.tn'], function () {
    Route::match(['get', 'post'],'/', 'AppController@homeMobile');
    // Route::get('showPhoto/{id}', 'AppController@showPhotoMobile');
    // Route::match(['get', 'post'], 'coverPhoto', 'AppController@coverPhotoMobile');
});

Route::match(['get', 'post'], '/', ['uses' => 'AppController@home', 'as' => 'home']);
// Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
// Route::post('signup', 'AppController@signup');
// Route::post('submitPhotos', 'AppController@submitPhotos');
// Route::post('upload', 'AppController@upload');
// Route::match(['get', 'post'], 'coverPhoto', 'AppController@coverPhoto');
// Route::match(['get', 'post'], 'showPhoto/{id}', 'AppController@showPhoto');
// Route::get('loadMore', 'AppController@loadMore');
// Route::get('getVideoXML/{id}', 'AppController@getVideoXML');
// Route::get('getPhoto/{filename}', ['uses' => 'AppController@getPhoto', 'as' => 'getPhoto']);
// Route::post('vote/{id}', 'AppController@vote');
// Route::post('report/{id}', 'AppController@reportPhoto');




/*Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);*/

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin'
], function()
{
    Route::get('/', ['uses' => 'AdminController@dashboard', 'as' => 'admin.dashboard']);
    Route::get('/banPhoto/{id}', 'AdminController@banPhoto');
});
