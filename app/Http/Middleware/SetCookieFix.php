<?php

namespace App\Http\Middleware;

use Closure;

class SetCookieFix
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        return $response->withCookie(cookie()->forever('cookie_fix', 1, null, null, false, false));
    }
}
