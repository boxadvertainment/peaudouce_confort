<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Participant;
use App\Photo;

class AdminController extends Controller
{
    public function dashboard(Request $request)
    {
        if ($request->input('username') != 'peaudouce') {
            return response('Unauthorized.', 401);
        }

        $participants = Participant::paginate(15);
        $photos = Photo::where('banned', false)->paginate(5);
        $count = Participant::count();
        $countWinners = Participant::has('photo')->count();

        $startDate = new \DateTime('05-10-2015');
        while ($startDate < (new \DateTime)) {
            $endDate = clone $startDate;
            $interval = [$startDate, $endDate->modify('+1 day')];
            $query = Participant::whereBetween('updated_at', $interval);
            $stat[$startDate->format('d/m/Y')]['participants'] = Participant::whereBetween('created_at', $interval)->count();
            $stat[$startDate->format('d/m/Y')]['gagnants'] = Participant::whereBetween('created_at', $interval)->has('photo')->count();
            $startDate->modify('+1 day');
        }

        return view('admin.dashboard', ['participants' => $participants, 'photos' => $photos, 'count' => $count, 'countWinners' => $countWinners, 'stat' => $stat]);
    }

    public function banPhoto($id)
    {
        $photo = Photo::find($id);
        $photo->banned = true;
        $photo->save();
    }

    public function login()
    {
        return true;
    }

}
