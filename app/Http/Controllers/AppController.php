<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Participant;
use App\Photo;
use App\Voter;
use App\Vote;
use Facebook;
use Config;

class AppController extends Controller
{
    private $captions = ['Je suis le chef', 'J’embrasse maman', 'Mon premier...', 'Je me brosse les dents'];

    public function homeMobile(Request $request)
    {
        return view('home_mob', ['captions' => $this->captions]);
    }

    public function home(Request $request)
    {
        $participant = Participant::find(session('participantId'));
        $alreadyExists = ($participant && $participant->photo && $participant->photo->cover) ? true : false;
        $photos = Photo::with('participant', 'votes')->whereNotNull('cover')->where('banned', false)->take($alreadyExists ? 15 : 14)->get();
		return view('home', ['photos' => $photos, 'alreadyExists' => $alreadyExists, 'captions' => $this->captions]);
    }

    public function loadMore(Request $request)
    {
        $query = Photo::with('participant', 'votes')->whereNotNull('cover')->where('banned', false);
        $participant = Participant::find(session('participantId'));
        if ($participant && $participant->photo && $participant->photo->cover) {
            $photos = $query->simplePaginate(15);
        } else {
            $page = (($request->input('page') - 1) * 15) -1;
            $result = $query->skip($page)->take(16)->get();
            $photos = new Paginator($result, 15, 1, ['path' => 'loadMore']);
        }
        return view('photosList', ['photos' => $photos]);
    }

    public function showPhoto($id)
    {
        $photo = Photo::where('banned', false)->findOrFail($id);

        $alreadyVoted = false;
        if (session()->has('voterId')) {
            $voterId = session('voterId');
            $alreadyVoted = $photo->votes->search(function($item, $key) use($voterId) {
                return $item->voter_id == $voterId;
            });
        }
        return view('showPhoto', ['photo' => $photo, 'alreadyVoted' => $alreadyVoted]);
    }

    public function showPhotoMobile($id)
    {
        $photo = Photo::where('banned', false)->findOrFail($id);
        return view('showPhoto_mob', ['photo' => $photo]);
    }

    public function reportPhoto($id)
    {
        $photo = Photo::findOrFail($id);
        $photo->reports++;
        $photo->save();
        return response()->json(['success' => true]);
    }

    public function getPhoto(Request $request, $filename)
    {
        $path = 'photos/' . ($request->input('size') ? $request->input('size') . '/' : '') . $filename;
        if (! \Storage::exists($path)) {
            abort(404);
        }

        return response(\Storage::get($path))->header('Content-Type', \Storage::mimeType($path));
	}

    public function getVideoXML($id)
    {
        $photo = Photo::findOrFail($id);
        return response()->view('videoXML', ['photo' => $photo])->header('Content-Type', 'application/xml');
    }

    public function signup(Request $request)
    {
        $validator = \Validator::make( $request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:participants',
            'phone' => 'regex:/^[0-9]{8}$/|unique:participants',
            'cin' => 'required|regex:/^[0-9]{8}$/|unique:participants'
        ]);

        $validator->setAttributeNames([
            'name' => 'Nom et prénom',
            'email' => 'Email',
            'phone' => 'Téléphone',
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $participant = session('participant');
        if ( ! $participant ) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $participant->name = $request->input('name');
        $participant->email = $request->input('email');
        $participant->phone = $request->input('phone');
        $participant->cin = $request->input('cin');
        $participant->ip = $request->ip();

        if ( $participant->save() ) {
            session(['participantId' => $participant->id]);
            return response()->json(['success' => true ]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function coverPhotoMobile(Request $request) {
        if ( ! session('participantId') ) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $photo = Participant::find(session('participantId'))->photo;

        if ($request->isMethod('get')) {
            return view('coverPhoto_mob', ['photo' => $photo]);
        }

        if ($request->input('cover') < 1 || $request->input('cover') > 10) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }

        $photo->cover = $request->input('cover');
        $photo->save();

        if ( $photo->save() ) {
            return redirect()->action('AppController@showPhotoMobile', [$photo->id]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function coverPhoto(Request $request) {
        if ( ! session('participantId') ) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $photo = Participant::find(session('participantId'))->photo;

        if ($request->isMethod('get')) {
            return view('coverPhoto', ['photo' => $photo]);
        }

        if ($request->input('cover') < 1 || $request->input('cover') > 10) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }

        $photo->cover = $request->input('cover');
        $photo->save();

        if ( $photo->save() ) {
            return redirect()->action('AppController@showPhoto', [$photo->id]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function upload(Request $request)
	{
        if ( ! session('participantId') ) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $rules['photo'] = 'required|max:2000|image';
        $rules['inputName'] = 'required';
        $rules['cropData'] = 'required';

		$validator = \Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $directory = 'photos';
        $path      = storage_path('app/' . $directory);
        $file      = $request->file('photo');
        // Si cette instruction se trouve dans une boucle on doit utiliser la fonction microtime() au lieu de time()
        $filename  = uniqid() . '_' . str_slug(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->guessExtension();
        $fileContents = \File::get($file);

        \Storage::put($directory . '/original/' . $filename,  $fileContents);

        $cropData = json_decode($request->input('cropData'));

        $img = \Image::make($fileContents);
        $img->crop($cropData->width, $cropData->height, $cropData->x, $cropData->y);
        $img->save($path . '/' . $filename);

        $thumbs = [
            [ 'width' => 520, 'height' => 325],
            [ 'width' => 245, 'height' => 158],
            [ 'width' => 184, 'height' => 115]
        ];
        foreach ($thumbs as $thumb) {
            $thumbDir = '/' . $thumb['width'] . 'x' . $thumb['height'];
            \Storage::makeDirectory($directory . $thumbDir);

            $img->resize($thumb['width'], $thumb['height']);
            $img->save($path . $thumbDir . '/' . $filename);
        }

		$uploads =  $request->session()->get('uploads', array());
		$uploads[$request->input('inputName')] = $filename;
		$request->session()->put('uploads', $uploads);

		return response()->json(['success' => true]);
	}

    public function submitPhotos(Request $request)
	{
        if ( ! session('participantId') ) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $rules = ['baby_name' => 'required'];
        for ($i=1; $i <= 5; $i++) {
            $rules['caption' . $i] = 'required';
        }
		$validator = \Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $participant = Participant::find(session('participantId'));
        if ($participant->photo) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }

        $photo = new Photo;
        $photo->baby_name = $request->input('baby_name');

        for ($i=1; $i <= 10; $i++) {
            $photo['photo' . $i] = $request->session()->get('uploads.photo' . $i);;
            $photo['caption' . $i] = $request->input('caption' . $i);
        }

        $photo = $participant->photo()->save($photo);

        return response()->json(['success' => true]);
	}

    public function vote(Request $request, $id)
	{
        


        return false;




        $photo = Photo::where('id', $id)->whereNotNull('cover')->where('banned', false)->firstOrFail();

        $fb = new Facebook\Facebook([
          'app_id' => Config::get('services.facebook.client_id'),
          'app_secret' => Config::get('services.facebook.client_secret'),
          'default_graph_version' => 'v2.4',
          'default_access_token' => $request->input('access_token'),
        ]);

        try {
          $response = $fb->get('/me?fields=id,name,gender,email');
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            return response('Graph returned an error: ' . $e->getMessage(), 500);
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            return response('Facebook SDK returned an error: ' . $e->getMessage(), 500);
        }

        $user        = $response->getGraphUser();
        $facebook_id = $user->getId();
        $name        = $user->getName();
        $gender      = $user->getProperty('gender');

        $voter = Voter::where('facebook_id', $facebook_id)->first();

        if (! $voter) {
            $voter = new Voter;
            $voter->name        = $name;
            $voter->facebook_id = $facebook_id;
            $voter->gender      = $gender;
            $voter->ip          = $request->ip();
            $voter->save();
        } else {
            $validator = \Validator::make([
                'id'=> $photo->id
            ], [
    			'id'   => 'unique:votes,photo_id,NULL,id,voter_id,' . $voter->id
    		]);

            if ( $validator->fails() ) {
                session(['voterId' => $voter->id]);
                return response('Vous avez déjà voté pour cette vidéo.', 500);
            }
        }

        session(['voterId' => $voter->id]);

		$vote = new Vote;
		$vote->voter()->associate($voter);
		$vote->photo()->associate($photo);
        if ($vote->save()) {
            return response()->json(['votes' => $photo->votes->count()]);
        }

        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
	}

}
