var gulp        = require('gulp');
var elixir      = require('laravel-elixir');
var browserSync = require('browser-sync').create('My server');
var dotenv      = require('dotenv').load();

var Task = elixir.Task;

elixir.extend('browserSync', function () {
    /* _____________________________________________________________________________________ */
    // Add these lines to node_modules/laravel-elixir/tasks/shared/Css.js
    // after .pipe(gulp.dest(options.output.baseDir))
    // =====================================================================================
    // .pipe(require('browser-sync').get('My server').stream({match: '**/*.css'}))
    /* _____________________________________________________________________________________ */

    var src = [
        'app/**/*',
        'public/**/*',
        'resources/views/**/*',
        '!public/css/*'
    ];

    gulp.task('serve', function () {
        browserSync.init({
            proxy: process.env.APP_URL
        });
    });

    new Task('browserSync', function() {
        if (browserSync.active === true) {
            browserSync.reload();
        }
    }).watch(src);
});

elixir(function(mix) {
    /* _____________________________________________________________________________________ */
    // modify line 91 ./node_modules/laravel-elixir/ingredients/version.js
    // return baseDir + '/build'; with return baseDir;
    /* _____________________________________________________________________________________ */
    mix
        .rubySass('main.scss', 'public/css/main.css')
        .rubySass('main_mobile.scss', 'public/css/main_mobile.css')
        .babel([
          'facebookUtils.js',
          'main.js'
        ], 'public/js/main.js')
        .babel([
          'facebookUtils.js',
          'main_mob.js'
        ], 'public/js/main_mob.js')
        .scripts([
            'bootstrap/dist/js/bootstrap.min.js',
            'sweetalert/dist/sweetalert.min.js',
            'cropper/dist/cropper.min.js',
            'jquery-form/jquery.form.js'
        ], 'public/js/vendor.js', 'vendor/bower_components' )
        .styles([
            'font-awesome/css/font-awesome.min.css',
            'bootstrap/dist/css/bootstrap.min.css',
            'sweetalert/dist/sweetalert.css',
            'animate.css/animate.min.css',
            'cropper/dist/cropper.min.css'
        ], 'public/css/vendor.css', 'vendor/bower_components')
        .copy('vendor/bower_components/modernizr/modernizr.js', 'public/js/modernizr.js')
        .copy('vendor/bower_components/jquery/dist/jquery.min.js', 'public/js/jquery.min.js')
        .copy('vendor/bower_components/font-awesome/fonts', 'public/fonts')
        //.version(['css/main.css', 'js/main.js'])
        .browserSync();
});
