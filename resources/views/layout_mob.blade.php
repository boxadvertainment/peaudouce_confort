<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="{{ App::getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title'){{ Config::get('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- <meta name="author" content="box.agency" /> -->

    @include('partials.socialMetaTags', [
        'title' => 'لحظة محبة',
        'description' => 'Participez au jeu concours de Dar Libero Peaudouce en partageant avec nous la photo de votre bout ‘chou et gagnez une année de couches Libero Peaudouce gratuite'
    ])

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main_mobile.css') }}">

    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ Request::url() }}';
        FB_APP_ID   = '{{ config('services.facebook.client_id') }}';
        FB_APP_PAGE = '{{ config('services.facebook.app_page') }}';
    </script>
</head>
<body class="@yield('class') mobile">

    <div class="loader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

    <header id="header-app">
        <div class="container">

            <img src="{{ asset('img/logo-app.png') }}" class="logo-app" width="180">
            <div class="logo-peaudouce">
                <a href="https://www.facebook.com/Dar.Libero.Peaudouce" target="_blank"><img src="{{ asset('img/logo-peaudouce.png') }}"  width="100px"></a>
            </div>

        </div>
    </header>

    <main class="main clearfix" role="main">
        <div class="container">
            @yield('content')
        </div>
    </main>

    @yield('body')

    <footer id="footer">
        <div class="content-wrapper">
        <a href="{{ asset('reglement.pdf') }}">Réglement du jeu</a>
        </div>
    </footer>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>

    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/main.js') }}?v=2"></script>

    @yield('scripts')

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-68440401-1', 'auto');
      ga('send', 'pageview');

    </script>
</body>
</html>
