@extends('layout')

@section('class', 'home')

@section('content')
    <div class="container">
        <div class="page landing-page row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Choisissez une des photos pour la couverture de votre album vidéo.</h1>
                <div class="cover-list">
                    @for($i=1; $i <= 10; $i++)
                        <div class="cover-item clearfix" data-index="{{ $i }}">
                            <img src="{{ url('getPhoto', [$photo['photo' . $i]] ) }}?size=184x115" alt="{{ $photo['caption' . $i] }}">
                        </div>
                    @endfor
                </div>

                <form id="cover-form" action="{{ action('AppController@coverPhoto') }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" class="cover-input" name="cover">
                    <button type="submit" class="btn btn-success validate-cover-btn" disabled>Valider</button>
                </form>
            </div>
        </div>
    </div>
@endsection