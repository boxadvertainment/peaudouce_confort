
@extends('layout')

@section('class', 'home')

@section('content')
    <div class="contain">
        <div class="page landing-page row {{ (count($errors) > 0) ? 'hide' : '' }}">
        @if($alreadyExists)
            <h1>La galerie des moments</h1>
            @foreach ($photos->chunk(5) as $chunk)
                <div class="row">
                    @foreach ($chunk as $photo)
                        <a href="{{ url('showPhoto', [$photo->id]) }}" class="baby-frame-wrapper clearfix">
                            <div class="description">
                                <h3>{{ str_limit($photo->participant->name, $limit = 19, $end = '...') }}</h3>
                                <p class="likes"><i class="fa fa-heart"></i> {{ $photo->votes->count() }}</p>
                            </div>
                            <div class="image-wrapper">
                                <div class="frame"></div>
                                <div class="image">
                                    <img src="{{ url('getPhoto', [$photo['photo' . $photo->cover]] ) }}?size=184x115" alt="{{ $photo->participant->name }}">
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            @endforeach
        @else
            <div class="row">
            @foreach ($photos->splice(0, 5)->all() as $photo)
                <a href="{{ url('showPhoto', [$photo->id]) }}" class="baby-frame-wrapper clearfix" data-id="{{ $photo->id }}">
                    <div class="description">
                        <h3>{{ str_limit($photo->participant->name, $limit = 19, $end = '...') }}</h3>
                        <p class="likes"><i class="fa fa-heart"></i> {{ $photo->votes->count() }}</p>
                    </div>
                    <div class="image-wrapper">
                        <div class="frame"></div>
                        <div class="image">
                            <img src="{{ url('getPhoto', [$photo['photo' . $photo->cover]] ) }}?size=184x115" alt="{{ $photo->participant->name }}">
                        </div>
                    </div>
                </a>
            @endforeach
            </div>

            <div class="row">

                <div class="one-col">
                    @foreach ($photos->splice(0, 2)->all() as $photo)
                        <a href="{{ url('showPhoto', [$photo->id]) }}" class="baby-frame-wrapper clearfix" data-id="{{ $photo->id }}">
                            <div class="description">
                                <h3>{{ str_limit($photo->participant->name, $limit = 19, $end = '...') }}</h3>
                                <p class="likes"><i class="fa fa-heart"></i> {{ $photo->votes->count() }}</p>
                            </div>
                            <div class="image-wrapper">
                                <div class="frame"></div>
                                <div class="image">
                                    <img src="{{ url('getPhoto', [$photo['photo' . $photo->cover]] ) }}?size=184x115" alt="{{ $photo->participant->name }}">
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>

                <div class="cta-box clearfix">
                    <button type="button" class="btn login-btn"> <i class="fa fa-facebook"></i> Commencer</button>
                    <p class="help">Pour savoir comment jouer <a href="#" title="Voir la vidéo"  data-toggle="modal" data-target="#howto-modal">cliquez içi <i class="fa fa-video-camera"></i></a></p>
                </div>

                <div class="one-col">
                    @foreach ($photos->splice(0, 2)->all() as $photo)
                        <a href="{{ url('showPhoto', [$photo->id]) }}" class="baby-frame-wrapper clearfix" data-id="{{ $photo->id }}">
                            <div class="description">
                                <h3>{{ str_limit($photo->participant->name, $limit = 19, $end = '...') }}</h3>
                                <p class="likes"><i class="fa fa-heart"></i> {{ $photo->votes->count() }}</p>
                            </div>
                            <div class="image-wrapper">
                                <div class="frame"></div>
                                <div class="image">
                                    <img src="{{ url('getPhoto', [$photo['photo' . $photo->cover]] ) }}?size=184x115" alt="{{ $photo->participant->name }}">
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>

            <div class="row">

                @foreach ($photos as $photo)
                    <a href="{{ url('showPhoto', [$photo->id]) }}" class="baby-frame-wrapper clearfix" data-id="{{ $photo->id }}">
                        <div class="description">
                            <h3>{{ str_limit($photo->participant->name, $limit = 19, $end = '...') }}</h3>
                            <p class="likes"><i class="fa fa-heart"></i> {{ $photo->votes->count() }}</p>
                        </div>
                        <div class="image-wrapper">
                            <div class="frame"></div>
                            <div class="image">
                                <img src="{{ url('getPhoto', [$photo['photo' . $photo->cover]] ) }}?size=184x115" alt="{{ $photo->participant->name }}">
                            </div>
                        </div>
                    </a>
                @endforeach

            </div>
            @endif
            <div class="row text-center">
                <a href="{{ url('loadMore') }}?page=2" class="btn btn-default load-more">Afficher plus</a>
            </div>
        </div>

        <div class="page upload-page {{ (count($errors) > 0) ? '' : 'hide' }}">
            <div>
                <h1>Choisissez vos photos</h1>
                <p>Pour un meilleur rendu, merci de fournir 10 photos avec une résolution min: 520x325px et 2Mo max</p>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form id="images-upload-form" action="{{-- action('AppController@submitPhotos') --}}" method="post" enctype="multipart/form-data">
                <div class="progress-container hide">
                    <div class="wrapper"><center>
                        <img src="{{ asset('img/c5.png') }}" alt="Confort taille 6">
                        <h3>Téléchargement des images en cours...</h3></center>
                        <div class="progress">
                          <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="min-width: 2em;">
                            0%
                          </div>
                        </div>
                    </div>
                </div>
                {!! csrf_field() !!}

                <div class="left-col">
                    @for($i=1; $i <= 10; $i++)
                        <div class="form-group upload-image-wrapper">
                            <label class="control-label">
                                <div class="number">{{$i}}</div>
                                <input type="text" name="caption{{ $i }}" value="" placeholder="{{ isset($captions[$i-1]) ? $captions[$i-1] : '' }}" class="label-image" maxlength="30" @if($i <= 5)
                                    required
                                @endif>
                            </label>
                            <fieldset class="img-upload">
                                <div class="img-preview-wrap">
                                    <div class="img-preview"></div>
                                    <div class="crop-controls">
                                        <div class="modif-controls">
                                            Modifier :
                                            <button type="button" class="delete-btn" data-toggle="tooltip" data-placement="top" title="Supprimer la photo"><i class="fa fa-times"></i> </button>
                                            <button type="button" class="crop-btn" data-toggle="tooltip" data-placement="top" title="Recadrer la photo"><i class="fa fa-crop"></i> </button>
                                        </div>
                                        <button type="button" class="btn add-btn" data-toggle="tooltip" data-placement="top" title="Choisissez une photo"></button>
                                    </div>
                                </div>
                                <input type="file" class="img-input" name="photo{{ $i }}" accept="image/*" data-minWidth="520" data-minHeight="325" required>
                                <input type="hidden" class="crop-data" name="cropData{{ $i }}">
                            </fieldset>
                        </div>
                    @endfor
                </div>

                <div class="right-col">

                    <div>
                        <h1>Comment ça mache?</h1>
                        <p>Pour créer la vidéo des meilleurs moments de votre bébé et tenter de gagner pleins de cadeaux avec Libero Peaudouce,
                        il vous suffit de télécharger 10 photos qui montrent ses différents moments de bonheur et d’inviter vos amis à voter pour lui.</p>
                    </div>
                    <div  id="fixed-bloc" class="sticky">
                    <div class="form-group">
                            <label class="control-label" for="baby-name"><h1>Prénom bébé: </h1></label>
                            <img src="img/arrow-baby-name.png">
                            <input type="text" class="form-controlbaby-name" id="baby-name" name="baby_name" required>
                        </div>

                        <button type="submit" class="btn submit-images" ></button>
                    </div>
                </div>


            </form>
        </div>
    </div>
@endsection

@section('body')
<div id="signup-modal" class="modal fade"  tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="signup-form" action="{{-- action('AppController@signup') }}--" method="post">

                <div class="modal-body clearfix">
                    <div class="">
                        <h2 class="modal-title" id="myModalLabel">Informations personnelles</h2>
                        <p class="info">Pour participer au jeu concours de Dar Libero Peaudouce, merci de remplir les champs ci-dessous:</p>
                        {!! csrf_field() !!}

                        <div class="form-group name">
                            <label class="control-label">Nom et prénom<span class="text-red">*</span> :</label>
                            <div class="inputs">
                                <input type="text" class="form-control" name="name" id="name" maxlength="30">
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group name">
                            <label class="control-label">Email<span class="text-red">*</span> :</label>
                            <div class="inputs">
                                <input type="text" class="form-control" name="email" id="email">
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group phone">
                            <label class=" control-label">Téléphone<span class="text-red">*</span> :</label>
                            <div class="inputs">
                                <div class="input-group">
                                    <div class="input-group-addon"><b>+216</b></div>
                                    <input type="text" class="form-control" name="phone" id="phone" maxlength="8">
                                </div>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group cin">
                            <label class="control-label">CIN<span class="text-red">*</span> :</label>
                            <div class="inputs">
                                <input type="text" class="form-control" name="cin" id="cin" maxlength="8">
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            En cliquant sur "Suivant", vous acceptez le <a href="{{ asset('reglement.pdf') }}" target="_blank">Règlement du jeu</a>
                        </div>

                        <button type="submit" class="btn btn-next">Suivant</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="crop-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

                <h2 class="modal-title">Recadrer l'image</h2>
                <div class="well">
                    <span class="fa-stack">
                      <i class="fa fa-square-o fa-stack-2x"></i>
                      <i class="fa fa-info fa-stack-1x"></i>
                    </span>
                    Pour recadrer cette image, redimensionnez la zone ci-dessous et cliquez sur "Valider".
                </div>
                <div class="crop-container">
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="validate-crop-btn btn btn-info">Valider</button>
                    <button type="button" class="cancel-crop-btn btn btn-default">Annuler</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
