<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="{{ App::getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title'){{ Config::get('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->

    @include('partials.socialMetaTags', [
        'title' => 'لحظة محبة',
        'description' => 'Participez au jeu concours de Dar Libero Peaudouce en partageant avec nous la photo de votre bout ‘chou et gagnez une année de couches Libero Peaudouce gratuite'
    ])

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ Request::url() }}';
        FB_APP_ID   = '{{ config('services.facebook.client_id') }}';
        FB_APP_PAGE = '{{ config('services.facebook.app_page') }}';

        window.top.location.href = 'https://www.facebook.com/Dar.Libero.Peaudouce';

        @if( ! App::environment('local') )
            // Redirect if not in facebook frame
            if ( top === self ) window.top.location.href = FB_APP_PAGE;

            // Add parameter "redirect" to page URL in App settings
            @if( Request::input('redirect') )
                window.top.location.href = FB_APP_PAGE;
            @endif

            @if ( ! Request::cookie('cookie_fix')  )
                function getCookie(cname) {
                    var name = cname + "=";
                    var ca = document.cookie.split(';');
                    for(var i=0; i<ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0)==' ') c = c.substring(1);
                        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
                    }
                    return '';
                }

                // Fix blocked third-party cookies in Safari
                if (getCookie('cookie_fix') === '') window.top.location.href = BASE_URL;
            @endif
        @endif
    </script>
</head>
<body class="@yield('class')">

    <div class="loader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

    <header id="header-app">
        <div class="content-wrapper">

            <a href="{{ route('home') }}"><img src="{{ asset('img/logo-app.png') }}" class="logo-app"></a>
            <div class="logo-peaudouce">
                <img src="{{ asset('img/label-logo.png') }}">
                <a href="https://www.facebook.com/Dar.Libero.Peaudouce" target="_blank"><img src="{{ asset('img/logo-peaudouce.png') }}"></a>
            </div>

        </div>
    </header>

    <main class="main clearfix" role="main">
        <div class="content-wrapper" style="display: none;">
            @yield('content')
        </div>
    </main>

    @yield('body')

    <footer id="footer">
        <div class="content-wrapper">
        <a href="{{ route('home') }}">Accueil</a> -
        <a href="#" data-toggle="modal" data-target="#howto-modal">Comment jouer</a> -
        <a href="#" data-toggle="modal" data-target="#gift-modal">Cadeaux</a> -
        <a href="{{ asset('reglement.pdf') }}" target="_blank">Réglement du jeu</a>
        </div>
    </footer>

    <div id="howto-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times-circle"></i></span></button>
                    <h4 class="modal-title">Comment participer au jeu </h4>
                </div>

                <div class="modal-body">

                    <iframe width="560" height="315" src="" frameborder="0" allowfullscreen id="videoHowTo"></iframe>

                </div>
            </div>
        </div>
    </div>

    <div id="gift-modal" class="modal fade" tabindex="1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle"></i></span></button>
                <img src="{{ asset('img/gifts.jpg') }}">
            </div>
        </div>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>

    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/main.js') }}?v=3"></script>

    @yield('scripts')

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-68440401-1', 'auto');
      ga('send', 'pageview');

    </script>
</body>
</html>
