@extends('layout_mob')

@section('class', 'home')

@section('content')
    <div class="containers">
        <div class="page photo-page" data-id="{{ $photo->id }}">
            <div class="half">
                <div class="photo-container clearfix">
                    <img src="{{ url('getPhoto', [$photo['photo' . $photo->cover]] ) }}?size=520x325" alt="{{ $photo->baby_name}}" />
                     <button class="btn btn-success share-btn">Partager</button>
                    <div class="votes"><span>{{ $photo->votes->count() }}</span>Votes</div>
                </div>
            </div>
            <div class="half image-des">
                <h2>Les moments de :</h2>
                <h1>{{ $photo->baby_name }}</h1>
                <div class="participant-info clearfix">
                    <div class="participant-picture">
                        <img src="https://graph.facebook.com/{{ $photo->participant->facebook_id }}/picture?type=square" alt="{{ $photo->participant->name }}" />
                    </div>

                    <div class="participant-name">
                        <span>proposé par:</span>
                        {{ $photo->participant->name }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('meta.title', 'Découvrez les moments de ' . $photo->baby_name)
@section('meta.description', 'Partager votre vidéo avec Libero Peaudouce afin de collecter le plus de votes et gagner une couverture de magazine avec votre Bébé.')
@section('meta.image', asset('img/fb-share-video.jpg'))
@section('meta.url', url('showPhoto', [$photo->id]))
