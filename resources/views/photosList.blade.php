@foreach ($photos->chunk(5) as $chunk)
    <div class="row">
        @foreach ($chunk as $photo)
            <a href="{{ url('showPhoto', [$photo->id]) }}" class="baby-frame-wrapper clearfix">
                <div class="description">
                    <h3>{{ $photo->participant->name }}</h3>
                    <p class="likes"><i class="fa fa-heart"></i> {{ $photo->votes->count() }}</p>
                </div>
                <div class="image-wrapper">
                    <div class="frame"></div>
                    <div class="image">
                        <img src="{{ url('getPhoto', [$photo['photo' . $photo->cover]] ) }}?size=184x115" alt="{{ $photo->participant->name }}">
                    </div>
                </div>
            </a>
        @endforeach
    </div>
@endforeach
@if($photos->hasMorePages())
    <div class="row text-center">
        <a href="{{ url('loadMore') }}?page={{ Request::input('page') + 1 }}" class="btn btn-default load-more">Afficher plus</a>
    </div>
@endif
