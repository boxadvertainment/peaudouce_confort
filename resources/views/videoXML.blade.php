<?xml version="1.0" encoding="utf-8"?>
<GALLERY TITLE="{!! htmlspecialchars($photo->baby_name) !!}">
	@for($i=1; $i <= 10; $i++)
		<IMAGE TITLE="{!! htmlspecialchars($photo['caption' . $i ]) !!}">{{ url('getPhoto', [$photo['photo' . $i]] ) }}?size=245x158</IMAGE>
	@endfor
</GALLERY>
