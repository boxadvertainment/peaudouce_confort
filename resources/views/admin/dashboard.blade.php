<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Deyson Bejarano">
    <title>Admin Panel - Peaudouce Confort</title>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

<body>
<div id="snippetContent" style="padding-top:10px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-10"><h1>Peaudouce Confort</h1></div>
        </div>
        <hr>
        <div class="alert alert-info" role="alert">
            Le nombre total des participants : <strong>{{ $count }}</strong> <br/>
            Le nombre total des videos : <strong>{{ $countWinners }}</strong>
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-3">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-warning"><strong>Nombre Participants / Videos par jour: </strong></li>
                    @foreach ($stat as $date => $value)
                        <li class="list-group-item"><span class="badge" style="background-color: rgba(0, 149, 255, 0.84)">{{ $value['gagnants'] }}</span> <span class="badge" style="background-color: rgba(255, 94, 0, 0.84)">{{ $value['participants'] }}</span> {{ $date }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-9">
                <ul class="nav nav-pills nav-justified">
                    <li role="presentation" class="@if(Request::input('tab') != 'photos') active @endif"><a href="?username=peaudouce&tab=participants">Participants</a></li>
                    <li role="presentation" class="@if(Request::input('tab') == 'photos') active @endif"><a href="?username=peaudouce&tab=photos">Videos</a></li>
                </ul>
                <br>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="participants">
                    @if(Request::input('tab') != 'photos')
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Nom et prénom</th>
                                <th>email</th>
                                <th>Téléphone</th>
                                <th>CIN</th>
                                <th>Date de création</th>
                            </thead>
                            <tbody>
                            @foreach($participants as $key => $participant)
                                <tr>
                                    <td>{{ ($participants->currentPage() - 1) * 15 + $key + 1}}</td>
                                    <td><a href="https://www.facebook.com/{{ $participant->facebook_id }}" target="_blank">
                                        <img src="http://graph.facebook.com/{{ $participant->facebook_id }}/picture?width=100" class="img-circle" width="30px" height="30px" alt=""/> {{ $participant->name . ' ' .  $participant->lastname}}</a></td>
                                    <td>{{ $participant->email }}</td>
                                    <td>{{ $participant->phone }}</td>
                                    <td>{{ $participant->cin }}</td>
                                    <td>{{ $participant->created_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $participants->appends(['username' => 'peaudouce', 'tab' => 'participants'])->render() !!}
                        </div>
                    @else
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Prénom bébé</th>
                                <th>Videos</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($photos as $key => $photo)
                                <tr>
                                    <td>{{ ($photos->currentPage() - 1) * 5 + $key + 1}}</td>
                                    <td>{{ $photo->baby_name }}</td>
                                    <td>
                                        @for($i = 1; $i <= 10; $i++)
                                            <img src="{{ url('getPhoto', [$photo['photo' . $i]] ) }}?size=184x115" alt="..." class="img-rounded" data-toggle="tooltip" data-placement="top" title="{{ $photo['caption' . $i] }}" >
                                        @endfor
                                    </td>
                                    <td>
                                        <a href="{{ action('Admin\AdminController@banPhoto', [$photo->id]) }}" type="button" class="btn btn-danger ban-btn">Bannir</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $photos->appends(['username' => 'peaudouce', 'tab' => 'photos'])->render() !!}
                        </div>
                    @endif
                    </div>
                  </div>

        </div><!--/col-9-->
    </div><!--/row-->
    <style type="text/css">
        body{margin-top:20px;}
    </style>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script>
    $('[data-toggle="tooltip"]').tooltip();
    $('.ban-btn').click(function(e) {
        $this = $(this);
        $.ajax({
          url: $this.attr('href'),
          method: 'GET'
        }).done(function(response) {
          alert('La photo a été bannie');
          location.reload();
        });
        e.preventDefault();
    });
</script>

</body>
</html>
