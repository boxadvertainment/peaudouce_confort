@extends('layout')

@section('class', 'home')

@section('content')
    <div class="containers">
        <div class="page photo-page" data-id="{{ $photo->id }}">
            <div class="half">
                <div class="photo-container clearfix">
                    <img src="{{ url('getPhoto', [$photo['photo' . $photo->cover]] ) }}?size=520x325" alt="{{ $photo->baby_name}}" />
                    <button class="btn btn-info show-video-btn">Voir la video</button>
                    <div class="votes"><span>{{ $photo->votes->count() }}</span>Votes</div>
                </div>
            </div>
            <div class="half image-des">
                <h2>Les moments de :</h2>
                <h1>{{ $photo->baby_name }}</h1>
                <div class="participant-info clearfix">
                    <div class="participant-picture">
                        <img src="https://graph.facebook.com/{{ $photo->participant->facebook_id }}/picture?type=square" alt="{{ $photo->participant->name }}" />
                    </div>

                    <div class="participant-name">
                        <span>proposé par:</span>
                        {{ $photo->participant->name }}
                    </div>
                </div>

                <p>Vous avez aimé les moments de {{ $photo->baby_name }} ? Votez et partagez sa vidéo pour lui permettre de gagner pleins de cadeaux avec Libero Peaudouce :</p>

                <ul>
                    <li>Un shooting photo bébé fait par des pros, votre bébé apparaitra sur la couverture du magazine Tunivisions</li>
                    <li>Un voyage à Disney Land Paris</li>
                    <li>Une année de consommation de couches gratuites</li>
                </ul>

                @if($alreadyVoted === false)
                    <button class="btn btn-info vote-btn">Voter</button>
                @endif
                <button class="btn btn-success share-btn">Partager</button>
                {{-- <button class="btn btn-info report-btn">Signaler</button> --}}

                <a href="{{ route('home') }}" class="back-home"><img src="{{ asset('img/btn-backtohome.png')}}"> Retourner à la galerie</a>
            </div>
        </div>
    </div>
@endsection

@section('body')
<div id="video-modal" class="modal fadeInDown" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog bounceIn">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">fermer <span aria-hidden="true">&times;</span></button>
                <div class="">
                    <object type="application/x-shockwave-flash" data="{{ asset('video.swf?s=2')}}" width="640px" height="385px">
                      <param name="allowscriptaccess" value="always" />
                      <param name="flashvars" value="idPhoto={{ $photo->id }}&baseUrl={{ url('/') }}" />
                    </object>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('meta.title', 'Découvrez les moments de ' . $photo->baby_name)
@section('meta.description', 'Partager votre vidéo avec Libero Peaudouce afin de collecter le plus de votes et gagner une couverture de magazine avec votre Bébé.')
@section('meta.image', asset('img/fb-share-video.jpg'))
@section('meta.url', url('showPhoto', [$photo->id]))
